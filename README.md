## Innoloft frontend challenge

### Task Highlights

- Login functionality done using mock data and api.
- Added required form fields inside the tabs.
- Updating data using redux and redux-saga.
- Configured redux-persist so that reviewer can reload and test if the data is updated or not.
- Integrated toast notifiactions.
- Added password strength checker.
- used css emotions as styled componets.
- Configured eslint and prettier.
- Configured husky for pre-commit tasks running.
- Used react-spring for mobile navigation
- Made UI Ultra responsive to support all possible devices.
- Used React router to navigate between login and my acconunt page.

### Time Required

- 16 Hours

### [Live Demo](http://3.130.129.250/)

http://3.130.129.250/

### Database to test

- email: "ben@innoloft.com",
- password: "innoloft123",

- email: "philipp@innoloft.com",
- password: "innoloft123",

- email: "s.pietsch@innoloft.com",
- password: "innoloft123",

## How to install locally

1. git clone [repo_url]
2. cd innoloft_frontend_application
3. npm install
4. yarn start
