import React from "react"
import {Provider as ReduxProvider} from "react-redux"
import configureStore from "./redux/configureStore"
import Routes from "./routes/Routes"

const {reduxStore} = configureStore()

const App = () => (
  <ReduxProvider store={reduxStore}>
    <Routes />
  </ReduxProvider>
)

export default App
