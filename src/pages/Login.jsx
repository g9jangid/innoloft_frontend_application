import React, {useState} from "react"
import styled from "@emotion/styled"
import EmailInput from "components/formFeilds/EmailInput"
import PasswordInput from "components/formFeilds/PasswordInput"
import {StyledLabel} from "assets/css/globalStyle"
import ButtonPrimary from "components/buttons/ButtonPrimary"
import emailValidator from "email-validator"
import {requestLogin} from "redux/modules/actions/authActions"
import {useDispatch} from "react-redux"
import {useHistory} from "react-router-dom"
import {ErrorText} from "assets/css/globalStyle"
import layout from "constants/layout"

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 60px;
  min-height: calc(100vh - 60px);
  flex-direction: column;
`

const StyledFormWrapper = styled.div`
  min-width: 650px;
  max-width: 60vw;
  margin-bottom: 20vh;
  @media (max-width: ${layout.tablet}) {
    min-width: 70vw;
    max-width: 70vw;
  }
  @media (max-width: ${layout.mobile}) {
    min-width: 90vw;
    max-width: 90vw;
  }
`

const StyledButtonWrapper = styled.div`
  margin-top: 40px;
  text-align: right;
`

const Login = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const [loginFormState, setLoginFormState] = useState({
    userEmail: "",
    emailError: "Please enter valid email.",
    isEmailInvalid: false,
    userPassword: "",
    passwordError: "Please enter valid password",
    isPasswordInvalid: false,
    formError: "",
  })

  const onEmailChange = (email) => {
    setLoginFormState({...loginFormState, userEmail: email})
  }

  const onPasswordChange = (password) => {
    setLoginFormState({...loginFormState, userPassword: password})
  }

  const onLogin = () => {
    let {userEmail, userPassword} = loginFormState
    if (emailValidator.validate(userEmail) && !!userPassword) {
      dispatch(
        requestLogin({
          email: userEmail,
          password: userPassword,
          date: Date.now(),
          callback: loginSuccess,
          errorCallback: loginFailed,
        })
      )
    } else {
      setLoginFormState({
        ...loginFormState,
        isEmailInvalid: !emailValidator.validate(userEmail),
        isPasswordInvalid: !userPassword,
      })
    }
  }

  const loginSuccess = () => {
    history.push("/app/my-account")
  }

  const loginFailed = () => {
    setLoginFormState({
      ...loginFormState,
      formError: "Incorrect Eamil or Password",
    })
  }

  return (
    <Container>
      <h1>Login</h1>
      <StyledFormWrapper>
        <StyledLabel style={{marginBottom: "8px", display: "block"}}>Email</StyledLabel>
        <EmailInput
          handleOnChange={(email) => onEmailChange(email)}
          errorText={loginFormState.emailError}
          isError={loginFormState.isEmailInvalid}
          value={loginFormState.userEmail}
        />
        <StyledLabel style={{marginBottom: "8px", marginTop: "32px", display: "block"}}>
          Password
        </StyledLabel>
        <PasswordInput
          handleOnChange={(password) => onPasswordChange(password)}
          errorText={loginFormState.passwordError}
          isError={loginFormState.isPasswordInvalid}
          value={loginFormState.userPassword}
        />
        <StyledButtonWrapper>
          <ButtonPrimary label="Login" handleOnClick={onLogin} />
          <ErrorText style={{display: "block", marginTop: "8px"}}>
            {loginFormState.formError}
          </ErrorText>
        </StyledButtonWrapper>
      </StyledFormWrapper>
    </Container>
  )
}

export default Login
