import React from "react"
import styled from "@emotion/styled"
import SideNav from "components/navigation/SideNav"
import {AppWrapper, AppContent} from "assets/css/globalStyle"
import {Tab, Tabs, TabList, TabPanel} from "react-tabs"
import AccountInfoForm from "containers/account/AccountInfoForm"
import PersonalInfoForm from "containers/account/PersonalInfoForm"
import layout from "constants/layout"

const TabListUlStyle = {
  listStyleType: "none",
  padding: 0,
  margin: "24px 0 0",
}
const TabLiStyle = {
  display: "inline-block",
  padding: "16px",
}

const TabButton = {margin: 0, cursor: "pointer"}

const StyledTabContainer = styled.div`
  flex: 1;
  padding: 0 24px 24px;
  @media (max-width: ${layout.tablet}) {
    padding: 0 0 24px;
  }
`
const StyledTabPanelWrapper = styled.div`
  min-height: calc(100vh - 220px);
  background-color: #fff;
  max-width: 900px;
  box-shadow: 2px 2px 15px 10px rgb(163 177 172 / 9%);
`

const Account = () => {
  return (
    <AppWrapper>
      <AppContent>
        <SideNav />
        <StyledTabContainer>
          <Tabs>
            <TabList style={TabListUlStyle}>
              <Tab style={TabLiStyle}>
                <h6 style={TabButton}>Account Information</h6>
              </Tab>
              <Tab style={TabLiStyle}>
                <h6 style={TabButton}>Personal Information</h6>
              </Tab>
            </TabList>
            <StyledTabPanelWrapper>
              <TabPanel>
                <AccountInfoForm />
              </TabPanel>
              <TabPanel>
                <PersonalInfoForm />
              </TabPanel>
            </StyledTabPanelWrapper>
          </Tabs>
        </StyledTabContainer>
      </AppContent>
    </AppWrapper>
  )
}

export default Account
