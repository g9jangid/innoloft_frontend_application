const users = [
  {
    token: "7N33hHZ26q4ksCaNl673",
    first_name: "Benjamin",
    last_name: "Sanger",
    email: "ben@innoloft.com",
    password: "innoloft123",
    street: "Sonnenallee 20",
    house_number: "22/143 Freiburg Stadt",
    zip: "79108",
    country: "Germany",
  },
  {
    token: "7oFOfBx4nO4DZCEqnvGU",
    first_name: "Philipp",
    last_name: "Bischoff",
    email: "philipp@innoloft.com",
    password: "innoloft123",
    street: "Sonnenallee 20",
    house_number: "22/143 Freiburg Stadt",
    zip: "79108",
    country: "Germany",
  },
  {
    token: "wnkp82usYEZXygYi1gPv",
    first_name: "Sven",
    last_name: "Pietsch",
    email: "s.pietsch@innoloft.com",
    password: "innoloft123",
    street: "Sonnenallee 20",
    house_number: "22/143 Freiburg Stadt",
    zip: "79108",
    country: "Germany",
  },
]

export default users
