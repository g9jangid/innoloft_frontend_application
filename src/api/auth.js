import userData from "api/mock-data/data"

export const login = ({email, password}) => {
  let data = userData.filter((user) => user.email == email && user.password == password)
  return {
    response: {
      status: !!data.length ? 200 : 400,
      statusMsg: !!data.length ? "Successfully Authenticated" : "No user found",
      data: !!data.length ? data[0] : {},
    },
  }
}
