import React, {useState} from "react"
import styled from "@emotion/styled"
import {StyledLabelSmall} from "assets/css/globalStyle"
import TextInput from "components/formFeilds/TextInput"
import DropDown from "components/formFeilds/DropDown"
import {useDispatch, useSelector} from "react-redux"
import ButtonPrimary from "components/buttons/ButtonPrimary"
import {toast} from "react-toastify"
import {requestUserDataUpdate} from "redux/modules/actions/userActions"
import layout from "constants/layout"

const Container = styled.div`
  padding: 40px;
  @media (max-width: ${layout.tablet}) {
    padding: 24px;
  }
`
const StyledButtonWrapper = styled.div`
  margin-top: 40px;
  text-align: right;
`

const StyledFormWrapper = styled.div`
  display: flex;
  @media (max-width: ${layout.mobile}) {
    flex-direction: column;
  }
`
const StyledFormWrapperChild = styled.div`
  flex: 1;
  @media (max-width: ${layout.mobile}) {
    margin: 0 !important;
  }
`

const PersonalInfoForm = () => {
  const dispatch = useDispatch()
  const userDetails = useSelector((state) => state.user)
  const [formState, setFormState] = useState({
    firstName: userDetails.first_name,
    isFirstNameTouched: false,
    lastName: userDetails.last_name,
    isLastNameTouched: false,
    street: userDetails.street,
    isStreetTouched: false,
    houseNumber: userDetails.house_number,
    isHouseNumberTouched: false,
    zip: userDetails.zip,
    isZipTouched: false,
    isZipValid: true,
    zipError: "Invalid ZIP/Postal Code",
    country: userDetails.country,
    error: "This field can not be empty.",
  })

  const onInputChange = (value, key) => {
    key === "zip"
      ? setFormState({...formState, [key]: value, isZipValid: validateZipCode(value)})
      : setFormState({...formState, [key]: value})
  }

  const validateZipCode = (zip) => {
    const regexZip = /^(?:\d{5}(?:[-]\d{4})?|\d{9})$/
    return regexZip.test(zip)
  }

  const onInputFocus = (key) => {
    setFormState({...formState, [key]: true})
  }

  const submitForm = () => {
    let {firstName, lastName, street, zip, isZipValid, houseNumber, country} = formState

    let isFormValid =
      !!firstName &&
      !!lastName &&
      !!street &&
      !!houseNumber &&
      !!zip &&
      isZipValid &&
      !!country

    if (isFormValid) {
      dispatch(
        requestUserDataUpdate({
          first_name: firstName,
          last_name: lastName,
          street: street,
          house_number: houseNumber,
          zip: zip,
          country: country,
        })
      )
      toast.success("Details updated successfully.")
    } else {
      toast.error("Please fix the errors!")
    }
  }

  return (
    <Container>
      <StyledFormWrapper>
        <StyledFormWrapperChild style={{flex: "1"}}>
          <StyledLabelSmall style={{marginBottom: "8px", display: "block"}}>
            First Name
          </StyledLabelSmall>
          <TextInput
            handleOnChange={(firstName) => onInputChange(firstName, "firstName")}
            errorText={formState.error}
            isError={!formState.firstName && formState.isFirstNameTouched}
            value={formState.firstName}
            handleOnFocus={() => onInputFocus("isFirstNameTouched")}
          />
        </StyledFormWrapperChild>
        <StyledFormWrapperChild style={{flex: "1", marginLeft: "24px"}}>
          <StyledLabelSmall style={{marginBottom: "8px", display: "block"}}>
            Last Name
          </StyledLabelSmall>
          <TextInput
            handleOnChange={(lastName) => onInputChange(lastName, "lastName")}
            errorText={formState.error}
            isError={!formState.lastName && formState.isLastNameTouched}
            value={formState.lastName}
            handleOnFocus={() => onInputFocus("isLastNameTouched")}
          />
        </StyledFormWrapperChild>
      </StyledFormWrapper>
      <StyledFormWrapper style={{marginTop: "24px"}}>
        <StyledFormWrapperChild style={{flex: "1"}}>
          <StyledLabelSmall style={{marginBottom: "8px", display: "block"}}>
            Street
          </StyledLabelSmall>
          <TextInput
            handleOnChange={(street) => onInputChange(street, "street")}
            errorText={formState.error}
            isError={!formState.street && formState.isStreetTouched}
            value={formState.street}
            handleOnFocus={() => onInputFocus("isStreetTouched")}
          />
        </StyledFormWrapperChild>
        <StyledFormWrapperChild style={{flex: "1", marginLeft: "24px"}}>
          <StyledLabelSmall style={{marginBottom: "8px", display: "block"}}>
            House/Row Number
          </StyledLabelSmall>
          <TextInput
            handleOnChange={(houseNumber) => onInputChange(houseNumber, "houseNumber")}
            errorText={formState.error}
            isError={!formState.houseNumber && formState.isHouseNumberTouched}
            value={formState.houseNumber}
            handleOnFocus={() => onInputFocus("isHouseNumberTouched")}
          />
        </StyledFormWrapperChild>
      </StyledFormWrapper>

      <StyledFormWrapper style={{justifyContent: "flex-end", marginTop: "24px"}}>
        <StyledFormWrapperChild
          style={{flex: "2", marginLeft: "48px"}}
        ></StyledFormWrapperChild>
        <StyledFormWrapperChild style={{flex: "1"}}>
          <StyledLabelSmall style={{marginBottom: "8px", display: "block"}}>
            Postal/Zip Code
          </StyledLabelSmall>
          <TextInput
            handleOnChange={(zip) => onInputChange(zip, "zip")}
            errorText={formState.zipError}
            isError={formState.isZipTouched && !formState.isZipValid}
            value={formState.zip}
            handleOnFocus={() => onInputFocus("isZipTouched")}
          />
        </StyledFormWrapperChild>
        <StyledFormWrapperChild style={{flex: "1", marginLeft: "24px"}}>
          <StyledLabelSmall style={{marginBottom: "8px", display: "block"}}>
            Country
          </StyledLabelSmall>
          <DropDown
            handleOnChange={(country) => onInputChange(country, "country")}
            value={formState.country}
          >
            <option value="Austria">Austria</option>
            <option value="Germany">Germany</option>
            <option value="Switzerland">Switzerland</option>
          </DropDown>
        </StyledFormWrapperChild>
      </StyledFormWrapper>

      <StyledButtonWrapper>
        <ButtonPrimary label="Update" handleOnClick={submitForm} />
      </StyledButtonWrapper>
    </Container>
  )
}

export default PersonalInfoForm
