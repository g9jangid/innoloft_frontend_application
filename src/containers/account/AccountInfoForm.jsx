import React, {useState} from "react"
import styled from "@emotion/styled"
import {StyledLabelSmall} from "assets/css/globalStyle"
import EmailInput from "components/formFeilds/EmailInput"
import PasswordInput from "components/formFeilds/PasswordInput"
import {useDispatch, useSelector} from "react-redux"
import ButtonPrimary from "components/buttons/ButtonPrimary"
import emailValidator from "email-validator"
import PasswordStrengthBar from "react-password-strength-bar"
import theme from "constants/theme"
import {toast} from "react-toastify"
import {requestUserDataUpdate} from "redux/modules/actions/userActions"
import layout from "constants/layout"
import passwordValidation from "utils/PasswordValidation"
import PasswordCriteriaChecker from "components/PasswordCriteriaChecker"

const Container = styled.div`
  padding: 40px;
  @media (max-width: ${layout.tablet}) {
    padding: 24px;
  }
`
const StyledButtonWrapper = styled.div`
  margin-top: 40px;
  text-align: right;
`

const StyledFormWrapper = styled.div`
  display: flex;
  @media (max-width: ${layout.mobile}) {
    flex-direction: column;
  }
`
const StyledFormWrapperChild = styled.div`
  flex: 1;
  @media (max-width: ${layout.mobile}) {
    margin: 0 !important;
  }
`

const StyledCheckIcon = styled.span`
  position: absolute;
  top: 10px;
  right: 20px;
  font-size: 22px;
  color: ${theme.success};
`
const StyledCrossIcon = styled.span`
  position: absolute;
  top: 10px;
  right: 20px;
  font-size: 22px;
  color: ${theme.error};
`

const AccountInfoForm = () => {
  const dispatch = useDispatch()
  const userDetails = useSelector((state) => state.user)
  const [formState, setFormState] = useState({
    email: userDetails.email,
    emailError: "Please enter valid email.",
    isEmailInvalid: false,
    password: "",
    isPasswordInvalid: false,
    confirmPassword: "",
    isPasswordMatched: false,
    confirmPasswordError: "Password not matched",
    isConfirmPasswordInvalid: false,
  })

  const onEmailChange = (email) => {
    setFormState({...formState, email: email})
  }

  const onEmailBlur = () => {
    let {email} = formState
    if (!!email) {
      setFormState({...formState, isEmailInvalid: !emailValidator.validate(email)})
    } else {
      setFormState({...formState, isEmailInvalid: false})
    }
  }

  const onEmailFocus = () => {
    setFormState({...formState, isEmailInvalid: false})
  }

  const onConfirmPasswordFocus = () => {
    setFormState({...formState, isConfirmPasswordInvalid: false})
  }

  const onPasswordChange = (password) => {
    let {confirmPassword} = formState
    setFormState({
      ...formState,
      password: password,
      isPasswordMatched: password === confirmPassword,
      isPasswordInvalid: !!password ? !passwordValidation(password).isValid : false,
    })
  }

  const onConfirmPasswordChange = (confirmPassword) => {
    let {password} = formState
    setFormState({
      ...formState,
      confirmPassword: confirmPassword,
      isPasswordMatched: password === confirmPassword,
    })
  }

  const onConfirmPasswordBlur = () => {
    let {confirmPassword, password} = formState
    if (!!confirmPassword && password !== confirmPassword) {
      setFormState({
        ...formState,
        isConfirmPasswordInvalid: true,
      })
    } else {
      setFormState({...formState, isConfirmPasswordInvalid: false})
    }
  }

  const submitForm = () => {
    let {
      email,
      password,
      confirmPassword,
      isPasswordMatched,
      isPasswordInvalid,
      isEmailInvalid,
    } = formState

    if (!isEmailInvalid && !isPasswordInvalid) {
      if (password !== confirmPassword) {
        toast.error("Password and confirm password did not match.")
        return false
      }
      if (isPasswordMatched && !!email) {
        if (password !== userDetails.password && email !== userDetails.email) {
          dispatch(
            requestUserDataUpdate({
              email,
              password,
            })
          )
          toast.success("Password and email successfully updated.")
        } else if (password !== userDetails.password) {
          dispatch(
            requestUserDataUpdate({
              password,
            })
          )
          toast.success("Password successfully updated.")
        } else if (email !== userDetails.email) {
          dispatch(
            requestUserDataUpdate({
              email,
            })
          )
          toast.success("Email successfully updated.")
        } else {
          toast.warn(
            "No data has been updated. Looks like you haven't added new details."
          )
        }
      } else if (isPasswordMatched) {
        if (password !== userDetails.password) {
          dispatch(
            requestUserDataUpdate({
              password,
            })
          )
          toast.success("Password successfully updated.")
        } else {
          toast.warn(
            "Please enter new password. Entered password matches with last 5 passwords in system."
          )
        }
      } else if (email) {
        if (email !== userDetails.email) {
          dispatch(
            requestUserDataUpdate({
              email,
            })
          )
          toast.success("Email successfully updated.")
        } else {
          toast.warn("Please enter new email. This is your current email.")
        }
      } else {
        toast.warn("Please enter any values in the form.")
      }
    } else {
      toast.error("Please fix the errors!")
    }
  }

  return (
    <Container>
      <StyledLabelSmall style={{marginBottom: "8px", display: "block"}}>
        Email
      </StyledLabelSmall>
      <EmailInput
        handleOnChange={(email) => onEmailChange(email)}
        errorText={formState.emailError}
        isError={formState.isEmailInvalid}
        value={formState.email}
        handleOnBlur={() => onEmailBlur()}
        handleOnFocus={() => onEmailFocus()}
      />
      <StyledFormWrapper>
        <StyledFormWrapperChild>
          <StyledLabelSmall
            style={{marginBottom: "8px", marginTop: "32px", display: "block"}}
          >
            Password
          </StyledLabelSmall>
          <PasswordInput
            handleOnChange={(password) => onPasswordChange(password)}
            isError={formState.isPasswordInvalid}
            value={formState.password}
          />
          {!!formState.password && (
            <PasswordStrengthBar
              password={formState.password}
              style={{marginTop: "8px"}}
            />
          )}
          {!!formState.password && (
            <PasswordCriteriaChecker
              invalidCriterias={passwordValidation(formState.password).invalidCriterias}
            />
          )}
        </StyledFormWrapperChild>
        <StyledFormWrapperChild style={{marginLeft: "24px"}}>
          <StyledLabelSmall
            style={{marginBottom: "8px", marginTop: "32px", display: "block"}}
          >
            Confirm Password
          </StyledLabelSmall>
          <div style={{position: "relative"}}>
            <PasswordInput
              handleOnChange={(confirmPassword) =>
                onConfirmPasswordChange(confirmPassword)
              }
              errorText={formState.confirmPasswordError}
              isError={formState.isConfirmPasswordInvalid}
              value={formState.confirmPassword}
              handleOnBlur={() => onConfirmPasswordBlur()}
              handleOnFocus={() => onConfirmPasswordFocus()}
            />
            {!!formState.confirmPassword &&
              (formState.isPasswordMatched ? (
                <StyledCheckIcon className="ion-checkmark-round"></StyledCheckIcon>
              ) : (
                <StyledCrossIcon className="ion-close-round"></StyledCrossIcon>
              ))}
          </div>
        </StyledFormWrapperChild>
      </StyledFormWrapper>

      <StyledButtonWrapper>
        <ButtonPrimary label="Update" handleOnClick={submitForm} />
      </StyledButtonWrapper>
    </Container>
  )
}

export default AccountInfoForm
