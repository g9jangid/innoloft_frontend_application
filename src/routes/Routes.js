import React from "react"
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom"
import {connect} from "react-redux"
import Login from "pages/Login"
import Account from "pages/Account"
import Header from "components/navigation/Header"
import {ToastContainer} from "react-toastify"

const Routes = ({isLoggedIn}) => (
  <Router>
    <Header isLoggedIn={isLoggedIn} />
    <Switch>
      <Route exact path="/">
        {!isLoggedIn ? (
          <Login />
        ) : (
          <Redirect
            to={{
              pathname: "/app/my-account",
            }}
          />
        )}
      </Route>
      <PrivateRoute path="/app/my-account" isLoggedIn={isLoggedIn}>
        <Account />
      </PrivateRoute>
    </Switch>
    <ToastContainer
      position="top-right"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      pauseOnHover
    />
  </Router>
)

const PrivateRoute = ({children, isLoggedIn}) => {
  return (
    <Route
      render={({location}) =>
        isLoggedIn ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: {from: location},
            }}
          />
        )
      }
    />
  )
}

export default connect(({auth}) => ({
  isLoggedIn: !!auth.accessToken,
}))(Routes)
