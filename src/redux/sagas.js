import {all} from "redux-saga/effects"
import {sagas as auth} from "./modules/sagas/authSagas"
import {sagas as user} from "./modules/sagas/userSagas"

export default function* rootSaga() {
  yield all([...auth, ...user].map((func) => func()).concat())
}
