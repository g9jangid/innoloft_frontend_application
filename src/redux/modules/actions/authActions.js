export const REQUEST_LOGIN = "auth/requestLogin"
export const SET_AUTH_DETAILS = "auth/setAuthDetails"
export const LOGOUT = "auth/logout"

export const requestLogin = (payload = {}) => ({
  type: REQUEST_LOGIN,
  payload,
})

export const setAuthDetails = (payload = {}) => ({
  type: SET_AUTH_DETAILS,
  payload,
})

export const logout = (payload = {}) => ({
  type: LOGOUT,
  payload,
})
