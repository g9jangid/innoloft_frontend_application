export const TOGGLE_NAVIGATION = "nav/toggleSideNavigation"

export const toggleSideNavigation = (payload = {}) => ({
  type: TOGGLE_NAVIGATION,
  payload,
})
