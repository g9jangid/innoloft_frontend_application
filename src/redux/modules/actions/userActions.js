export const REQUEST_USER_DATA_UPDATE = "user/requestUserDataUpdate"
export const SET_USER_DATA = "user/setUserData"

export const requestUserDataUpdate = (payload = {}) => ({
  type: REQUEST_USER_DATA_UPDATE,
  payload,
})

export const setUserData = (payload = {}) => ({
  type: SET_USER_DATA,
  payload,
})
