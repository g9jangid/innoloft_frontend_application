import {combineReducers} from "redux"
import auth from "./reducers/authReducer"
import user from "./reducers/userReducer"
import sideNav from "./reducers/sideNavReducer"

export default combineReducers({
  auth,
  user,
  sideNav,
})
