import {Auth} from "api/"
import {call, put, takeLatest} from "redux-saga/effects"
import {REQUEST_LOGIN, setAuthDetails} from "../actions/authActions"
import {setUserData} from "../actions/userActions"

function* authenticateUser({payload: {email, password, date, callback, errorCallback}}) {
  try {
    const {response} = yield call(Auth.login, {
      email,
      password,
      date,
    })
    if (response.status == 200) {
      let {token, ...rest} = response.data
      yield put(setAuthDetails({accessToken: token, lastLoggedIn: date}))
      yield put(setUserData({...rest}))
      !!callback && callback()
    } else {
      errorCallback && errorCallback()
    }
  } catch (error) {
    //error : something went wrong
  }
}

function* watchUserLogin() {
  yield takeLatest(REQUEST_LOGIN, authenticateUser)
}

export const sagas = [watchUserLogin]
