import {put, takeLatest} from "redux-saga/effects"
import {REQUEST_USER_DATA_UPDATE, setUserData} from "../actions/userActions"

function* updateUserData({payload}) {
  try {
    yield put(setUserData(payload))
  } catch (error) {
    //error : something went wrong
  }
}

function* watchUserDataUpdate() {
  yield takeLatest(REQUEST_USER_DATA_UPDATE, updateUserData)
}

export const sagas = [watchUserDataUpdate]
