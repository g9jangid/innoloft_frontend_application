import {SET_AUTH_DETAILS} from "../actions/authActions"
import {LOGOUT} from "../actions/authActions"

export default (state = {}, {payload, type}) => {
  switch (type) {
    case SET_AUTH_DETAILS:
      return {...state, ...payload}
    case LOGOUT:
      return {}
    default:
      return state
  }
}
