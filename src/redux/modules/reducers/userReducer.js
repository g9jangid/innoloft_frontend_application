import {SET_USER_DATA} from "../actions/userActions"
import {LOGOUT} from "../actions/authActions"

export default (state = {}, {payload, type}) => {
  switch (type) {
    case SET_USER_DATA:
      return {...state, ...payload}
    case LOGOUT:
      return {}
    default:
      return state
  }
}
