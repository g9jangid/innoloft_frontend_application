import {TOGGLE_NAVIGATION} from "../actions/navigationAction"

export default (state = false, {type}) => {
  switch (type) {
    case TOGGLE_NAVIGATION:
      return !state
    default:
      return state
  }
}
