export default [
  {
    label: "Home",
    icon: "ion-ios-home-outline",
    href: "/",
  },
  {
    label: "My Account",
    icon: "ion-person",
    href: "/",
  },
  {
    label: "My Company",
    icon: "ion-briefcase",
    href: "/",
  },
  {
    label: "My Settings",
    icon: "ion-gear-b",
    href: "/",
  },
  {
    label: "News",
    icon: "ion-ios-paper",
    href: "/",
  },
  {
    label: "Analytics",
    icon: "ion-ios-analytics",
    href: "/",
  },
]
