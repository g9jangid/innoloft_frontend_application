export default {
  wrapperMaxWidth: "85vw",
  wrapperWidth: "calc(100vw - 32px)",
  wrapperMargin: "16px",
  sideNavWidth: "240px",
  tablet: "1024px",
  mobile: "768px",
}
