import React, {useEffect, useState} from "react"
import theme from "constants/theme"
import layout from "constants/layout"
import styled from "@emotion/styled"
import Links from "constants/sideNavLinks"
import ButtonSolid from "components/buttons/ButtonSolid"
import {useDispatch, useSelector} from "react-redux"
import {logout} from "redux/modules/actions/authActions"
import {Spring} from "react-spring/renderprops"

const NavContainer = styled.div`
  width: ${layout.sideNavWidth};
  height: calc(100vh - 60px);
  overflow-y: scroll;
  @media (max-width: ${layout.tablet}) {
    width: 70vw;
    max-width: 70vw;
    position: fixed;
    top: 60px;
    width: 70vw;
    background-color: ${theme.white};
    padding: 0 20px;
    z-index: 10;
    bottom: 0;
    box-shadow: 0 0 225px 220px rgb(0 0 0 / 47%);
  }
`

const StyledMenuUl = styled.ul`
  margin: 0;
  padding: 40px 0;
  list-style: none;
  list-style-type: none;
`
const StyledMenuLi = styled.li`
  margin: 0;
`
const StyledMenuLink = styled.a`
  color: ${theme.textDark};
  display: flex;
  padding: 8px;
  align-items: center;
  border-radius: 4px;
  text-decoration: none;
  cursor: pointer;
  &:hover {
    background-color: rgb(0 0 0 / 5%);
  }
`
const StyledMenuIcon = styled.span`
  font-size: 20px;
  padding-right: 8px;
`

const SideNav = () => {
  const dispatch = useDispatch()
  const sideNav = useSelector((state) => state.sideNav)
  const [isDesktop, setIsDesktop] = useState(
    window.innerWidth > layout.tablet.slice(0, -2)
  )
  const handleResize = () => {
    setIsDesktop(window.innerWidth > layout.tablet.slice(0, -2))
  }

  useEffect(() => {
    window.addEventListener("resize", handleResize)
  })

  return sideNav || isDesktop ? (
    <Spring from={{opacity: 0, left: "-100%"}} to={{opacity: 1, left: "0"}}>
      {(props) => (
        <NavContainer style={props}>
          <StyledMenuUl>
            {Links.map((link, i) => (
              <StyledMenuLi key={i}>
                <StyledMenuLink href={link.href}>
                  <StyledMenuIcon className={link.icon}></StyledMenuIcon>
                  {link.label}
                </StyledMenuLink>
              </StyledMenuLi>
            ))}
          </StyledMenuUl>
          <ButtonSolid
            handleOnClick={() => dispatch(logout())}
            label="Logout"
            icon="ion-log-out"
          />
        </NavContainer>
      )}
    </Spring>
  ) : null
}

export default SideNav
