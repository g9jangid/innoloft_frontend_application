import React, {useEffect, useState} from "react"
import theme from "constants/theme"
import layout from "constants/layout"
import styled from "@emotion/styled"
import logo_primary from "assets/images/logo_innoloft.svg"
import logo_white from "assets/images/innoloft.svg"
import ButtonLink from "components/buttons/ButtonLink"
import {useDispatch, useSelector} from "react-redux"
import {logout} from "redux/modules/actions/authActions"
import {toggleSideNavigation} from "redux/modules/actions/navigationAction"

const NavContainer = styled.div`
  background-color: ${(props) => (props.isLoggedIn ? theme.primary : theme.white)};
  box-shadow: 1px 1px 6px 0 rgba(0, 0, 0, 0.2);
  position: fixed;
  width: 100%;
  height: 60px;
  left: 0;
  top: 0;
  right: 0;
  z-index: 100;
  display: flex;
  justify-content: center;
`
const StyledMenuIcon = styled.span`
  color: ${theme.white};
  font-size: 24px;
  margin: 0 8px;
`
const NavWrapper = styled.div`
  max-width: ${layout.wrapperMaxWidth};
  width: ${layout.wrapperWidth};
  display: flex;
  justify-content: space-between;
`
const LogoImage = styled.img`
  height: 20px;
  margin: 20px 0;
`
const StyledDrawerOpenIcon = styled.span`
  font-size: 50px;
  color: ${theme.white};
  margin-right: 16px;
  line-height: 1;
  display: inline-block;
`
const StyledCloseIcon = styled.span`
  font-size: 30px;
  color: ${theme.white};
  margin-right: 16px;
  line-height: 1;
  display: inline-block;
`

const Header = ({isLoggedIn}) => {
  const dispatch = useDispatch()
  const sideNav = useSelector((state) => state.sideNav)
  const [isDesktop, setIsDesktop] = useState(
    window.innerWidth > layout.tablet.slice(0, -2)
  )
  const handleResize = () => {
    setIsDesktop(window.innerWidth > layout.tablet.slice(0, -2))
  }

  useEffect(() => {
    window.addEventListener("resize", handleResize)
  })

  return (
    <NavContainer isLoggedIn={isLoggedIn}>
      <NavWrapper>
        <div style={{display: "flex", alignItems: "center"}}>
          {!isDesktop &&
            (!sideNav ? (
              <StyledDrawerOpenIcon
                className="ion-navicon"
                onClick={() => dispatch(toggleSideNavigation())}
              ></StyledDrawerOpenIcon>
            ) : (
              <StyledCloseIcon
                className="ion-close-round "
                onClick={() => dispatch(toggleSideNavigation())}
              ></StyledCloseIcon>
            ))}
          <LogoImage src={isLoggedIn ? logo_white : logo_primary} />
        </div>
        {isLoggedIn && (
          <div style={{display: "flex", alignItems: "center"}}>
            <StyledMenuIcon className="ion-ios-bell"></StyledMenuIcon>
            <StyledMenuIcon className="ion-ios-email"></StyledMenuIcon>
            <ButtonLink
              handleOnClick={() => dispatch(logout())}
              label="Logout"
              icon="ion-log-out"
            />
          </div>
        )}
      </NavWrapper>
    </NavContainer>
  )
}

export default Header
