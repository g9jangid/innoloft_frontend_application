import React from "react"
import styled from "@emotion/styled"
import {StyledSelect, ErrorText} from "assets/css/globalStyle"

const Container = styled.div`
  display: flex;
  flex-direction: column;
`

const DropDown = ({children, value, handleOnChange, isError = false, errorText = ""}) => (
  <Container>
    <StyledSelect
      value={value}
      isError={isError}
      onChange={(e) => handleOnChange(e.target.value)}
    >
      {children}
    </StyledSelect>
    {isError && <ErrorText>{errorText}</ErrorText>}
  </Container>
)

export default DropDown
