import React from "react"
import styled from "@emotion/styled"
import {StyledTextArea, ErrorText} from "assets/css/globalStyle"

const Container = styled.div`
  display: flex;
  flex-direction: column;
`

const onBlur = (handleOnBlur) => {
  handleOnBlur && handleOnBlur()
}

const onFocus = (handleOnFocus) => {
  handleOnFocus && handleOnFocus()
}

const TextArea = ({
  placeholder = "",
  value,
  handleOnChange,
  handleOnBlur,
  handleOnFocus,
  isError = false,
  errorText = "",
}) => (
  <Container>
    <StyledTextArea
      placeholder={placeholder}
      isError={isError}
      onChange={(e) => handleOnChange(e.target.value)}
      onBlur={() => onBlur(handleOnBlur)}
      onFocus={() => onFocus(handleOnFocus)}
    >
      {value}
    </StyledTextArea>
    {isError && <ErrorText>{errorText}</ErrorText>}
  </Container>
)

export default TextArea
