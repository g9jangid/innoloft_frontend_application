import React from "react"
import styled from "@emotion/styled"
import {StyledInput, ErrorText} from "assets/css/globalStyle"

const Container = styled.div`
  display: flex;
  flex-direction: column;
`

const onBlur = (handleOnBlur) => {
  handleOnBlur && handleOnBlur()
}

const onFocus = (handleOnFocus) => {
  handleOnFocus && handleOnFocus()
}

const EmailInput = ({
  placeholder = "",
  value,
  handleOnChange,
  handleOnBlur,
  handleOnFocus,
  isError = false,
  errorText = "",
}) => (
  <Container>
    <StyledInput
      type="email"
      placeholder={placeholder}
      value={value}
      isError={isError}
      onChange={(e) => handleOnChange(e.target.value)}
      onBlur={() => onBlur(handleOnBlur)}
      onFocus={() => onFocus(handleOnFocus)}
    />
    {isError && <ErrorText>{errorText}</ErrorText>}
  </Container>
)

export default EmailInput
