import React from "react"
import styled from "@emotion/styled"
import theme from "constants/theme"

const StyledP = styled.p`
  margin: 0;
  font-size: 14px;
  opacity: 0.7;
  color: ${(props) => (props.isValid ? theme.success : theme.error)};
  span {
    color: ${(props) => (props.isValid ? theme.success : theme.error)};
  }
`

const IconStyle = {
  fontSize: "12px",
  paddingRight: "4px",
}

const PasswordCriteriaChecker = ({invalidCriterias}) => {
  let isMinLength = !invalidCriterias.includes("min")
  let isUpperCase = !invalidCriterias.includes("uppercase")
  let isLowerCase = !invalidCriterias.includes("lowercase")
  let isDigits = !invalidCriterias.includes("digits")
  let isSymbols = !invalidCriterias.includes("symbols")

  return (
    <div>
      <StyledP isValid={isMinLength}>
        <span
          style={IconStyle}
          className={isMinLength ? "ion-checkmark-round" : "ion-close-round"}
        ></span>{" "}
        At least 8 characters in length
      </StyledP>
      <StyledP isValid={isUpperCase && isLowerCase}>
        <span
          style={IconStyle}
          className={
            isUpperCase && isLowerCase ? "ion-checkmark-round" : "ion-close-round"
          }
        ></span>{" "}
        Use upper and lower case characters
      </StyledP>
      <StyledP isValid={isDigits}>
        <span
          style={IconStyle}
          className={isDigits ? "ion-checkmark-round" : "ion-close-round"}
        ></span>{" "}
        At least 1 number
      </StyledP>
      <StyledP isValid={isSymbols}>
        <span
          style={IconStyle}
          className={isSymbols ? "ion-checkmark-round" : "ion-close-round"}
        ></span>{" "}
        At least 1 special character
      </StyledP>
    </div>
  )
}

export default PasswordCriteriaChecker
