import React from "react"
import styled from "@emotion/styled"
import theme from "constants/theme"

const StyledButton = styled.button`
  height: 45px;
  padding: 0 40px;
  border: 1px solid ${theme.borderDarkGray};
  font-weight: 400;
  color: ${theme.textDark};
  cursor: pointer;
  &:hover {
    background-color: ${theme.primary};
    color: ${theme.white};
  }
`

const onClick = (handleOnClick) => {
  handleOnClick && handleOnClick()
}

const ButtonPrimary = ({handleOnClick, label}) => (
  <StyledButton onClick={() => onClick(handleOnClick)}>{label}</StyledButton>
)

export default ButtonPrimary
