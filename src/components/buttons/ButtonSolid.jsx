import React from "react"
import styled from "@emotion/styled"
import theme from "constants/theme"

const StyledButton = styled.button`
  height: 38px;
  padding: 0px 38px;
  font-weight: 600;
  color: ${theme.white};
  font-size: 14px;
  border: none;
  line-height: 1;
  border-radius: 4px;
  background-color: ${theme.primary};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  &:hover {
    background-color: #535fb9;
  }

  span {
    color: ${theme.white};
    padding-right: 8px;
    font-size: 20px;
  }
`

const onClick = (handleOnClick) => {
  handleOnClick && handleOnClick()
}

const ButtonSolid = ({handleOnClick, label, icon}) => (
  <StyledButton onClick={() => onClick(handleOnClick)}>
    {!!icon && <span className={icon}></span>}
    {label}
  </StyledButton>
)

export default ButtonSolid
