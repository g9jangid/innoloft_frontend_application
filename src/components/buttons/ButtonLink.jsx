import React from "react"
import styled from "@emotion/styled"
import theme from "constants/theme"

const StyledButton = styled.button`
  padding: 0px 16px;
  font-weight: 600;
  color: ${theme.white};
  font-size: 14px;
  border: none;
  line-height: 1;
  border-radius: 4px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    background-color: rgb(255 255 255 / 12%);
  }

  span {
    color: ${theme.white};
    padding-right: 8px;
    font-size: 20px;
  }
`

const onClick = (handleOnClick) => {
  handleOnClick && handleOnClick()
}

const ButtonLink = ({handleOnClick, label, icon}) => (
  <StyledButton onClick={() => onClick(handleOnClick)}>
    {!!icon && <span className={icon}></span>}
    {label}
  </StyledButton>
)

export default ButtonLink
