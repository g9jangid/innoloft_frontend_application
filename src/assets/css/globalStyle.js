import theme from "constants/theme"
import layout from "constants/layout"
import styled from "@emotion/styled"

export const StyledInput = styled.input`
  height: 50px;
  background-color: ${theme.bgInput};
  padding: 8px 16px;
  border: none;
  border-bottom: 2px solid
    ${(props) => (props.isError ? theme.error : theme.borderDarkGray)};
  &:focus {
    background-color: rgb(243 243 243 / 34%);
  }
`

export const StyledTextArea = styled.textarea`
  height: 50px;
  background-color: ${theme.bgInput};
  padding: 8px 16px;
  border: none;
  border-bottom: 2px solid
    ${(props) => (props.isError ? theme.error : theme.borderDarkGray)};
  &:focus {
    background-color: rgb(243 243 243 / 34%);
  }
`

export const StyledSelect = styled.select`
  height: 50px;
  background-color: ${theme.bgInput};
  padding: 8px 16px;
  border: none;
  border-bottom: 2px solid
    ${(props) => (props.isError ? theme.error : theme.borderDarkGray)};
`
export const ErrorText = styled.span`
  color: ${theme.error};
`
export const StyledLabel = styled.label`
  font-size: 20px;
  font-weight: 600;
  color: ${theme.label};
`

export const StyledLabelSmall = styled.label`
  font-size: 16px;
  font-weight: 400;
  color: ${theme.label};
`

export const AppWrapper = styled.div`
  width: 100vw;
  min-height: calc(100vh - 60px);
  margin-top: 60px;
  background-color: ${theme.bgLight};
  display: flex;
  justify-content: center;
`
export const AppContent = styled.div`
  max-width: ${layout.wrapperMaxWidth};
  width: ${layout.wrapperWidth};
  display: flex;
`
